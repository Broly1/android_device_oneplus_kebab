#
# Copyright (C) 2022 The PixelExperience Project
#
# SPDX-License-Identifier: Apache-2.0
#

LOGO_IMAGES := $(wildcard device/oneplus/kebab/custom-logo/$(PRODUCT_DEVICE)/*)

AB_OTA_PARTITIONS += \
    $(foreach f, $(notdir $(LOGO_IMAGES)), $(basename $(f)))

