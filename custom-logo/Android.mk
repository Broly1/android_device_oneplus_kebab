#
# Copyright (C) 2022 Paranoid Android
# Copyright (C) 2022 The PixelExperience Project
#
# SPDX-License-Identifier: Apache-2.0
#

LOCAL_PATH := $(call my-dir)

ifneq ($(filter kebab,$(PRODUCT_DEVICE)),)

$(info Including logo for $(PRODUCT_DEVICE)...)

LOGO_IMAGES := $(wildcard $(LOCAL_PATH)/$(PRODUCT_DEVICE)/*)

$(foreach f, $(notdir $(LOGO_IMAGES)), \
    $(call add-radio-file,$(PRODUCT_DEVICE)/$(f)))

endif

